connection: "snowlooker"

# include all the views
include: "/views/**/*.view"

label: "Srikar Looker Learning"

datagroup: getting_started_looker_sri_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: getting_started_looker_sri_default_datagroup
#this is comment

access_grant: can_view_explore_orderitems{
  user_attribute: can_view_order_items
  allowed_values: ["%"]
}

explore: distribution_centers {}

explore: etl_jobs {}

explore: events {
  join: users {
    type: left_outer
    sql_on: ${events.user_id} = ${users.id} ;;
    relationship: many_to_one
  }
}

explore: inventory_items {
  join: products {
    type: left_outer
    sql_on: ${inventory_items.product_id} = ${products.id} ;;
    relationship: many_to_one
  }

  join: distribution_centers {
    type: left_outer
    sql_on: ${products.distribution_center_id} = ${distribution_centers.id} ;;
    relationship: many_to_one
  }
}

explore: order_items {
  required_access_grants: [can_view_explore_orderitems]
  access_filter: {
    field: order_items.created_date
    user_attribute: can_view_created_date
  }

  join: users {
    type: left_outer
    sql_on: ${order_items.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: inventory_items {
    type: left_outer
    sql_on: ${order_items.inventory_item_id} = ${inventory_items.id} ;;
    relationship: many_to_one
  }

  join: products {
    type: left_outer
    sql_on: ${inventory_items.product_id} = ${products.id} ;;
    relationship: many_to_one
  }

  join: distribution_centers {
    type: left_outer
    sql_on: ${products.distribution_center_id} = ${distribution_centers.id} ;;
    relationship: many_to_one
  }
  join: customer_behavior {
    type: left_outer
    sql_on: ${order_items.user_id}=${customer_behavior.user_id} ;;
    relationship: many_to_one
  }
}

explore: products {
  join: distribution_centers {
    type: left_outer
    sql_on: ${products.distribution_center_id} = ${distribution_centers.id} ;;
    relationship: many_to_one
  }
}
explore: customer_behavior {
  view_label: "Cust1 Behavior"
}
explore: users {
  conditionally_filter: {filters: [id: "123"]
    unless: [created_date]}
}
