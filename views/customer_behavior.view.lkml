view: customer_behavior {
  # # You can specify the table name if it's different from the view name:
  # sql_table_name: my_schema_name.tester ;;
  #
  label: "Cust2_behavior"
  derived_table: {
    sql: SELECT user_id,
         count(*) as lifetime_order,
         SUM(sale_price) as lifetime_revenue,
         MIN(DATE(created_at)) as first_order_date,
         MAX(DATE(created_at)) as latest_order_date

        FROM
        order_items
        GROUP BY
        user_id
        ;;
  }

  dimension: user_id {

    primary_key: yes
    type: number
    sql: ${TABLE}.user_id ;;
  }

  measure: count_order {
    type: count_distinct
    sql: ${TABLE}.lifetime_order ;;
  }
dimension: customer_lifetime_orders {
    description: "The total number of orders that a customer has placed since first using the website"
    type: tier
    tiers: [1,2,5,9]
    sql: ${TABLE}.lifetime_order ;;
    style: integer
  }
  dimension: customer_lifetime_revenue {
    description: "The total amount of revenue brought in from an individual customer over the course of their patronage"
    type: tier
    tiers: [5,20,50,100,500,1000]
    value_format_name: usd_0
    sql: ${TABLE}.lifetime_revenue;;
    style: integer
  }
  measure: total_lifetime_orders {
    description: "The total number of orders placed over the course of customers’ lifetimes."
    type: sum
    sql: ${TABLE}.lifetime_order ;;
  }

  measure: average_lifetime_orders {
    description: "The average number of orders that a customer places over the course of their lifetime as a customer."
    type: average
    sql: ${TABLE}.lifetime_order ;;


  }

  measure: total_lifetime_revenue {
    description: "The total amount of revenue brought in over the course of customers’ lifetimes."
    type: sum
    sql:${TABLE}.lifetime_revenue ;;
    #value_format_name: usd
    value_format: "0.##"
  }

  measure: average_lifetime_revenue {
    description: "The average amount of revenue that a customer brings in over the course of their lifetime as a customer."
    type: average
    sql: ${TABLE}.lifetime_revenue ;;
    #value_format_name: usd
    value_format: "0.##"
  }

  dimension_group: first_order_date{
    description: "The date in which a customer placed his or her first order on the fashion.ly website"
    type: time
    timeframes: [raw,date, month, year, quarter]
    sql: ${TABLE}.first_order_date ;;
  }

  dimension_group: latest_order_date{
    description: "The date in which a customer placed his or her most recent order on the fashion.ly website"
    type: time
    timeframes: [raw,date, month, year, quarter]
    sql: ${TABLE}.latest_order_date ;;
  }

  dimension: is_active{
    description: "Identifies whether a customer is active or not (has purchased from the website within the last 90 days)"
    type: yesno
    sql: DATEDIFF(day,  ${latest_order_date_raw},CURRENT_DATE())<90 ;;

  }
  dimension: no_of_last_order {
    description: "The number of days since a customer placed his or her most recent order on the website"
    view_label: "Days since last order"
    type: number
    sql: DATEDIFF(day, CURRENT_DATE(), ${latest_order_date_raw}) ;;

  }

  measure: avg_days_last_order{
    description: "The average number of days since customers have placed their most recent orders on the website"
    type: average
    view_label: "Average Days Since Latest Order"
    sql: ${no_of_last_order} ;;
  }

  measure: repeat_customer {
    description: "Identifies whether a customer was a repeat customer or not"
    view_label: "Is a repeat Customer"
    type: yesno
    sql: ${TABLE}.lifetime_order>1 ;;
  }
  measure: count {
    type: count_distinct
    sql: ${user_id} ;;
  }




 }
