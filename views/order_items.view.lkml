view: order_items {
  sql_table_name: "PUBLIC"."ORDER_ITEMS"
    ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}."ID" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year,
      hour_of_day, day_of_week, month_num,  week_of_year, month_name
    ]
    sql: ${TABLE}."CREATED_AT" ;;
  }

  dimension_group: delivered {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year,hour_of_day, day_of_week, month_num,  week_of_year
    ]
    sql: ${TABLE}."DELIVERED_AT" ;;
  }

  dimension: inventory_item_id {
    type: number
    # hidden: yes
    sql: ${TABLE}."INVENTORY_ITEM_ID" ;;
  }

  dimension: order_id {
    type: number
    sql: ${TABLE}."ORDER_ID" ;;
  }

  dimension_group: returned {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."RETURNED_AT" ;;
  }

  dimension: sale_price {
    type: number
    sql: ${TABLE}."SALE_PRICE" ;;
  }

  dimension_group: shipped {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."SHIPPED_AT" ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}."STATUS" ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}."USER_ID" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      users.first_name,
      users.last_name,
      users.id,
      inventory_items.id,
      inventory_items.product_name
    ]
  }
  measure: total_sale_price {
    type:  sum
    description: "Total sales from items sold"
    sql: ${sale_price} ;;
    value_format_name: usd
    drill_fields: [user_details*]
  }

  measure: average_sale_price {
    type: average
    description: "Average sale price of items sold"
    sql: ${sale_price} ;;
    value_format_name: usd
  }
  measure: cummulative_total_sales {
    type: running_total
    description: "Cumulative total sales from items sold (also known as a running total)"
    sql: ${sale_price} ;;
    value_format_name: usd
  }
  measure: total_gross_revenue {
    type: sum
    description: "Total revenue from completed sales (cancelled and returned orders excluded)"
    sql: ${sale_price} ;;
    drill_fields: [product_details*]
    filters:
     [status: "-Cancelled,-Returned"]

  }
  measure: average_gross_revenue {
    type: average
    description: "Average revenue from completed sales (cancelled and returned orders excluded)"
    sql: ${sale_price} ;;
    filters: [status: "Complete"]
  }
  measure: total_gross_margin_amount {
    type: number
    description: "Total difference between the total revenue from completed sales and the cost of the goods that were sold"
    sql: ${total_gross_revenue}-${inventory_items.total_cost} ;;
  }
measure: average_gross_margin {
  type: number
  description: "Average difference between the total revenue from completed sales and the cost of the goods that were sold"
  sql: ${average_gross_revenue}-${inventory_items.avg_cost} ;;
}
measure: gross_margin_percent {
  type: number
  description: "Total Gross Margin Amount / Total Gross Revenue"
  sql:  ${total_gross_margin_amount}/${total_gross_revenue};;
}
measure: number_of_items_returned {
  type: count
  description: "Number of items that were returned by dissatisfied customers"
  #sql: ${inventory_item_id};;
  filters: [returned_year: "-NULL"]
}

measure: item_return_rate {
  type: number
  description: "Number of Items Returned / total number of items sold"
  sql: ${number_of_items_returned}/ ${inventory_items.number_of_items_sold};;
}
measure: number_of_customers_returning_items {
  type: count_distinct
  description: "Number of users who have returned an item at some point"
  sql: ${user_id} ;;
  filters: [returned_year: "-NULL"]
}

measure: number_of_customers {
  type: count_distinct
  description: "Number of Unique customers"
  sql: ${user_id} ;;
}

measure: percent_of_users_returned {
  type: number
  description: "Number of Customer Returning Items / total number of customers"
  sql: ${number_of_customers_returning_items}/${number_of_customers} ;;

}
measure: average_spend_per_customer {
  type: number
  description: "Total Sale Price / total number of customers"
  sql: ${total_sale_price}/${number_of_customers} ;;
}
  set: user_details {
    fields: [user_id,users.age_group,users.gender]
  }
  set: product_details {
    fields: [products.category, products.brand]
  }

  measure: total_gross_revenue2 {
    type: sum
    description: "Total revenue from completed sales (cancelled and returned orders excluded)"
    sql: ${sale_price} ;;

    filters: {
      field: status
      value: "Complete"
    }

  }

  dimension_group: days_delivered {
    type: duration
    intervals: [day, week]
    sql_start: ${created_raw} ;;
    sql_end: ${delivered_raw} ;;
  }

  measure: avg_delivery_days {
    type: average
    sql: ${days_days_delivered} ;;
  }
  measure: average_cate_orders {
    description: "The average number of orders that a customer places over the course of their lifetime as a customer."
    type: average
    sql: ${products.category} ;;
  }





}
