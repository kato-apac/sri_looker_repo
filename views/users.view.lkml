view: users {
  sql_table_name: "PUBLIC"."USERS"
    ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}."ID" ;;
  }

  dimension: age {
    type: number
    sql: ${TABLE}."AGE" ;;
  }

  dimension: city {
    type: string

    sql: ${TABLE}."CITY" ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}."COUNTRY" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year,hour_of_day, month_num
    ]
    sql: ${TABLE}."CREATED_AT" ;;
  }
measure: checking {
  type: number
}
  dimension: email {
    type: string
    sql: ${TABLE}."EMAIL" ;;
  }

  dimension: first_name {
    type: string
    sql: ${TABLE}."FIRST_NAME" ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}."GENDER" ;;
  }

  dimension: last_name {
    type: string
    sql: ${TABLE}."LAST_NAME" ;;
  }

  dimension: latitude {
    type: number
    sql: ${TABLE}."LATITUDE" ;;
  }

  dimension: longitude {
    type: number
    sql: ${TABLE}."LONGITUDE" ;;
  }

  dimension: state {
    type: string
    map_layer_name: "us_states"
    sql: ${TABLE}."STATE" ;;
  }

  dimension: traffic_source {
    type: string
    sql: ${TABLE}."TRAFFIC_SOURCE" ;;
  }

  dimension: zip {
    type: zipcode
    sql: ${TABLE}."ZIP" ;;
  }

  measure: count {
    type: count
    drill_fields: [id, first_name, last_name, events.count, order_items.count]
  }
  dimension: age_group {
    type: tier
    tiers: [15,26,36,51,66]
    style: integer
    sql: ${age} ;;
    }

    dimension: new_customer {
      description: "If the customer account created in lessthan 90 days"
      type: yesno
      sql: DATEDIFF(day,CURRENT_DATE(), ${created_raw})<90;;
    }

    dimension_group: time_since_signup {
      type: duration
      intervals: [day,month]
      sql_start: ${created_raw} ;;
      sql_end: (SELECT CURRENT_DATE()) ;;
    }

    measure: avg_num_days_since_signup {
      view_label: "Average Number of days since sign up"
      type: average
      sql: ${days_time_since_signup} ;;
    }

    measure: avg_num_months_since_signup {
      view_label: "Average number of months since sign up"
      type: average
      sql: ${months_time_since_signup} ;;
    }

    measure: dummy_1 {


      type: count
    }

}
